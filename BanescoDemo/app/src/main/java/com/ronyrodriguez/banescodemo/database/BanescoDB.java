package com.ronyrodriguez.banescodemo.database;

public class BanescoDB {

    public static final String DATABASE_NAME = "BanescoDB.db";
    public static final int DATABASE_VERSION = 1;
    public static final String CREATE_USER_TABLE = "CREATE TABLE " + UserContract.TABLE_NAME +
            " (" + UserContract.COLUMN_ID + " INTEGER PRIMARY KEY," +
            UserContract.COLUMN_NAME + " TEXT," +
            UserContract.COLUMN_PASSWORD + " TEXT)";
    public static final String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + UserContract.TABLE_NAME;

    public class UserContract {
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PASSWORD = "password";
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_ID = "_id";

        private UserContract() {
        }
    }
}
