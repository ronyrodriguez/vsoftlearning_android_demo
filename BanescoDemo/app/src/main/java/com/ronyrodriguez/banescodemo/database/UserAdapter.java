package com.ronyrodriguez.banescodemo.database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ronyrodriguez.banescodemo.R;

import java.util.List;

public class UserAdapter extends ArrayAdapter<User> {

    List<User> users;

    public UserAdapter(Context context, int resourceId,
                       List<User> objects) {
        super(context, resourceId, objects);

        users = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater vi =
                    LayoutInflater.from(getContext());
            ;
            v = vi.inflate(R.layout.customlistview_item, null);
        }
        User item = users.get(position);

        //TextView nameView = (TextView) v.findViewById(R.id.nameTextView);
        //TextView phoneView = (TextView) v.findViewById(R.id.phoneTextView);
        //nameView.setText(item.getName());
        //phoneView.setText(item.getPassword());
        return v;
    }

}
