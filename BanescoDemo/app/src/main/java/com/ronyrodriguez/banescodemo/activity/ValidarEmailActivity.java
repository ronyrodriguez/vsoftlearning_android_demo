package com.ronyrodriguez.banescodemo.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.ronyrodriguez.banescodemo.R;
import com.ronyrodriguez.banescodemo.model.Email;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by RoNy on 11/30/2017.
 */

public class ValidarEmailActivity extends AppCompatActivity {

    //https://mailboxlayer.com/

    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validar);

        progressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
        progressBar.setVisibility(View.VISIBLE);

        GetData getData = new GetData();
        getData.execute();
    }


    public class GetData extends AsyncTask<String, String, String> {

        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            String apikey = "";

            try {
                URL url = new URL("https://apilayer.net/api/check?access_key=" + apikey + "&email=support@apilayer.com");

                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            progressBar.setVisibility(View.GONE);

            Gson gson = new Gson();
            Email email = gson.fromJson(result, Email.class);

            int x = 0;
        }

    }

}
