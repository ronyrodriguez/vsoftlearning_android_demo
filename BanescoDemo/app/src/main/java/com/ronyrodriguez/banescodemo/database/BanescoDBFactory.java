package com.ronyrodriguez.banescodemo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BanescoDBFactory extends SQLiteOpenHelper {

    public BanescoDBFactory(Context context) {
        super(context, BanescoDB.DATABASE_NAME, null, BanescoDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(BanescoDB.CREATE_USER_TABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL(BanescoDB.DROP_USER_TABLE);
        onCreate(db);
    }
}
