package com.ronyrodriguez.banescodemo.model;

/**
 * Created by RoNy on 11/30/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Email implements Serializable {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("did_you_mean")
    @Expose
    private String didYouMean;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("domain")
    @Expose
    private String domain;
    @SerializedName("format_valid")
    @Expose
    private Boolean formatValid;
    @SerializedName("mx_found")
    @Expose
    private Boolean mxFound;
    @SerializedName("smtp_check")
    @Expose
    private Boolean smtpCheck;
    @SerializedName("catch_all")
    @Expose
    private Boolean catchAll;
    @SerializedName("role")
    @Expose
    private Boolean role;
    @SerializedName("disposable")
    @Expose
    private Boolean disposable;
    @SerializedName("free")
    @Expose
    private Boolean free;
    @SerializedName("score")
    @Expose
    private Float score;
    private final static long serialVersionUID = 1389212232935262440L;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDidYouMean() {
        return didYouMean;
    }

    public void setDidYouMean(String didYouMean) {
        this.didYouMean = didYouMean;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getFormatValid() {
        return formatValid;
    }

    public void setFormatValid(Boolean formatValid) {
        this.formatValid = formatValid;
    }

    public Boolean getMxFound() {
        return mxFound;
    }

    public void setMxFound(Boolean mxFound) {
        this.mxFound = mxFound;
    }

    public Boolean getSmtpCheck() {
        return smtpCheck;
    }

    public void setSmtpCheck(Boolean smtpCheck) {
        this.smtpCheck = smtpCheck;
    }

    public Boolean getCatchAll() {
        return catchAll;
    }

    public void setCatchAll(Boolean catchAll) {
        this.catchAll = catchAll;
    }

    public Boolean getRole() {
        return role;
    }

    public void setRole(Boolean role) {
        this.role = role;
    }

    public Boolean getDisposable() {
        return disposable;
    }

    public void setDisposable(Boolean disposable) {
        this.disposable = disposable;
    }

    public Boolean getFree() {
        return free;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

}


