package com.ronyrodriguez.banescodemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ronyrodriguez.banescodemo.R;
import com.ronyrodriguez.banescodemo.database.User;
import com.ronyrodriguez.banescodemo.database.UserManager;

import java.util.List;

/**
 * Created by RoNy on 11/30/2017.
 */

public class LoginActivity extends AppCompatActivity {

    private final LoginActivity _activity = this;

    public UserManager manager;

    private String mName = "";
    private String mPass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Intent intent = new Intent(get_activity(), ValidarEmailActivity.class);
        //startActivity(intent);


        final EditText etUser = (EditText) findViewById(R.id.et_user);
        final EditText etPass = (EditText) findViewById(R.id.et_pass);

        Button button = (Button) findViewById(R.id.bt_aceptar);
        Button button2 = (Button) findViewById(R.id.bt_cancel);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String textUser = etUser.getText().toString();

                String textPass = etPass.getText().toString();

                if (textUser.equalsIgnoreCase("")) {
                    Toast t = Toast.makeText(_activity, "El usario es requerido", Toast.LENGTH_LONG);
                    t.show();
                    return;
                }

                if (textPass.equalsIgnoreCase("")) {
                    Toast t = Toast.makeText(_activity, "La contraseña es requerido", Toast.LENGTH_LONG);
                    t.show();
                    return;
                }

                if (textUser.equalsIgnoreCase(mName) && textPass.equalsIgnoreCase(mPass)) {
                    Intent intent = new Intent(get_activity(), MainActivity.class);
                    intent.putExtra("user", etUser.getText().toString());
                    intent.putExtra("pass", etPass.getText().toString());
                    startActivity(intent);
                } else {
                    Toast t = Toast.makeText(_activity, "El usuario/contrasena invalido", Toast.LENGTH_LONG);
                    t.show();
                    return;
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        starDataBase();
    }


    public void starDataBase() {
        manager = new UserManager(this);
        manager.open();
        List<User> listado = manager.getAllUsersObject();
        if (listado.size() == 0) {
            long id = manager.insertUser("rony", "1234567");
            User user = manager.getUserObject(id);
            mName = user.getName();
            mPass = user.getPassword();
            Toast t = Toast.makeText(_activity, "Base de datos inicializada", Toast.LENGTH_LONG);
            t.show();
        } else {
            User user = listado.get(0);
            mName = user.getName();
            mPass = user.getPassword();
        }
    }


    public LoginActivity get_activity() {
        return _activity;
    }

}
