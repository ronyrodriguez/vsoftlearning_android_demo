package com.ronyrodriguez.banescodemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.ronyrodriguez.banescodemo.database.BanescoDB.UserContract;

import java.util.ArrayList;
import java.util.List;

public class UserManager {

    SQLiteDatabase db;
    BanescoDBFactory dbHelper;
    //Context context;


    public UserManager(Context context) {
        //this.context=context;
        dbHelper = new BanescoDBFactory(context);
    }

    public BanescoDBFactory open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return dbHelper;
    }

    public void close() {
        dbHelper.close();
    }

    public long insertUser(String name, String phoneNumber) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(UserContract.COLUMN_NAME, name);
        initialValues.put(UserContract.COLUMN_PASSWORD, phoneNumber);
        return db.insert(UserContract.TABLE_NAME,
                null, initialValues);
    }

    public long insertUser(User user) {

        return insertUser(user.getName(), user.getPassword());
    }

    public boolean deleteUser(long rowId) {
        return db.delete(UserContract.TABLE_NAME,
                BanescoDB.UserContract.COLUMN_ID +
                        "=" + rowId, null) > 0;
    }

    public Cursor getAllUsers() {
        return db.query(UserContract.TABLE_NAME, new String[]
                        {UserContract.COLUMN_ID,
                                UserContract.COLUMN_NAME,
                                UserContract.COLUMN_PASSWORD},
                null, null, null, null, null);
    }

    public List<User> getAllUsersObject() {
        Cursor cCursor = getAllUsers();
        List<User> users = new ArrayList<User>();
        while (cCursor.moveToNext()) {
            User user = new User();
            user.setId(cCursor.getLong(0));
            user.setName(cCursor.getString(1));
            user.setPassword(cCursor.getString(2));
            users.add(user);
        }
        return users;
    }

    public Cursor getUser(long rowId) throws SQLException {
        Cursor cCursor = db.query(UserContract.TABLE_NAME, new String[]
                        {UserContract.COLUMN_ID,
                                UserContract.COLUMN_NAME,
                                UserContract.COLUMN_PASSWORD},
                UserContract.COLUMN_ID + "=" + rowId, null, null, null, null);

        if (cCursor != null) {
            cCursor.moveToFirst();
        }
        return cCursor;
    }

    public User getUserObject(long rowId) throws SQLException {
        Cursor cCursor = getUser(rowId);
        User user = new User();
        user.setId(rowId);
        user.setName(cCursor.getString(1));
        user.setPassword(cCursor.getString(2));
        return user;
    }

    public boolean updateUser(long rowId, String name, String phoneNumber) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(UserContract.COLUMN_NAME, name);
        initialValues.put(UserContract.COLUMN_PASSWORD, phoneNumber);
        return db.update(UserContract.TABLE_NAME,
                initialValues,
                UserContract.COLUMN_ID + "=" + rowId, null) > 0;
    }

    public boolean updateUser(User user) {
        return updateUser(user.getId(), user.getName(), user.getPassword());
    }
}
